package com.students;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Student> students = new ArrayList<>();

        students.add(new Student(1, "John", "Smith", "Brown",
                1990,"Memphis", "+14158519136",
                "Engineering", "3", "FLA-301"));
        students.add(new Student(2, "Jack", "Jackson", "Bob",
                1992,"Miami", "+18665403229",
                "Natural sciences", "5", "NSF-502"));
        students.add(new Student(3, "Mia", "Levi", "Maria",
                2000,"Chicago", "+19183614018",
                "Agriculture", "1", "FOA-104"));
        students.add(new Student(4, "Luis", "Mark", "Antonio",
                1997,"Texas", "+18777838899",
                "Pharmacy", "4", "FOP-403"));

        System.out.println(students);
    }
}
