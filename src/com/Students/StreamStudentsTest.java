package com.students;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StreamStudentsTest {

    List<Student> students = new ArrayList<>();
    List<Student> expectedFaculty = new ArrayList<>();
    List<Student> expectedFacultyAndCourse = new ArrayList<>();
    List<Student> expectedYearOfBirth = new ArrayList<>();

    @BeforeEach
    public void createTestArray() {
        Student johnSmith = new Student(1, "John", "Smith", "Brown", 1990,
                "Memphis", "+14158519136", "Engineering", "3", "FLA-301");
        Student jackJackson = new Student(2, "Jack", "Jackson", "Bob", 1992,
                "Miami", "+18665403229", "Natural sciences", "5", "NSF-502");
        Student miaLevi = new Student(3, "Mia", "Levi", "Maria", 2000,
                "Chicago", "+19183614018", "Agriculture", "1", "FOA-104");
        Student luisMark = new Student(4, "Luis", "Mark", "Antonio", 1997,
                "Texas", "+18777838899", "Pharmacy", "4", "FOP-403");
        Student rickSpringfield = new Student(5, "Rick", "Springfield", "Gray", 2000,
                "Memphis", "+13444576874", "Engineering", "4", "FLA-401");
        Student billyBrown = new Student(6, "Billy", "Brown", "Nick", 1995,
                "Memphis", "+13444566874", "Engineering", "4", "FLA-401");

        students.add(johnSmith);
        students.add(jackJackson);
        students.add(miaLevi);
        students.add(luisMark);
        students.add(rickSpringfield);
        students.add(billyBrown);

        expectedFaculty.add(johnSmith);
        expectedFaculty.add(rickSpringfield);
        expectedFaculty.add(billyBrown);

        expectedFacultyAndCourse.add(rickSpringfield);
        expectedFacultyAndCourse.add(billyBrown);

        expectedYearOfBirth.add(miaLevi);
        expectedYearOfBirth.add(rickSpringfield);
    }

    @Test
    void showStudentsByFaculty() {
        //given
        String faculty = "Engineering";

        //when
        List<Student> actual = StreamStudents.studentsByFaculty(students, faculty);
        List<Student> expected = expectedFaculty;

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void showStudentsByFacultyAndCourse() {
        //given
        String faculty = "Engineering";
        String course = "4";

        //when
        List<Student> actual = StreamStudents.studentsByFacultyAndCourse(students, faculty, course);
        List<Student> expected = expectedFacultyAndCourse;

        //then
        assertEquals(expected, actual);
    }

    @Test
    void studentsByYearOfBirth() {
        //given
        int yearOfBirth = 2000;

        //when
        List<Student> actual = StreamStudents.studentsByYearOfBirth(students, yearOfBirth);
        List<Student> expected = expectedYearOfBirth;

        //then
        assertEquals(expected, actual);
    }

    @Test
    void studentsByNameInGroup() {
        //given
        String group = "FLA-401";

        //when
        String expected = "[Springfield Rick Gray, Brown Billy Nick]";
        List<String> actual = StreamStudents.studentsByNameInGroup(students, group);

        //then
        assertEquals(expected, String.valueOf(actual));
    }

    @Test
    void studentsByGroup() {
        //given

        //when
        String expected = "[Smith John Brown - Engineering FLA-301, Jackson Jack Bob - Natural sciences NSF-502, "
                + "Levi Mia Maria - Agriculture FOA-104, Mark Luis Antonio - Pharmacy FOP-403, " +
                "Springfield Rick Gray - Engineering FLA-401, Brown Billy Nick - Engineering FLA-401]";
        List<String> actual = StreamStudents.studentsByGroup(students);

        //then
        assertEquals(expected, String.valueOf(actual));
    }

    @Test
    void countStudentsOnFaculty() {
        //given
        String faculty = "Pharmacy";

        //when
        long expected = 1;
        long actual = StreamStudents.studentsByCountOnFaculty(students, faculty);

        //then
        assertEquals(expected, actual);
    }
}